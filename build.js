const consola = require('consola');
const fs = require('fs');
const path = require('path');
const os = require('os');
const {spawn} = require('child_process');

const buildDir = path.join(os.tmpdir(), 'troll-engine', 'build', 'browser');

new Promise(function (resolve, reject) {
    fs.mkdir(buildDir, {recursive: true}, (err) => {
        if (err) reject(err);
        resolve();
    })
})
    .then(() => {
        return new Promise((resolve, reject) => {
            const cmake = spawn('emconfigure', [
                `cmake`,
                `CMAKE_INSTALL_PREFIX=${__dirname}`,
                path.join(__dirname, 'src', 'ogre-1.12.1')
            ], {cwd: buildDir});

            cmake.stdout.on('data', (data) => {
                consola.log(`stdout: ${data}`)
            });

            cmake.on('error', (err) => {
                consola.error(`Error: ${err}`);
                throw err;
            });

            cmake.stderr.on('data', (data) => {
                consola.error(`stderr: ${data}`)
            });

            cmake.on('close', (code) => {
                consola.log(`Exited with code: ${code}`);
                if (code === 0) resolve();
            });
        });
    })
    .then(() => {
        /*
        return new Promise((resolve, reject) => {
            const cmake = spawn('cmake', [
                `CMAKE_INSTALL_PREFIX=${__dirname}`,
                path.join(__dirname, 'src', 'ogre-1.12.1')
            ], {cwd: buildDir});

            cmake.stdout.on('data', (data) => {
                consola.log(`stdout: ${data}`)
            });

            cmake.on('error', (err) => {
                consola.error(`Error: ${err}`);
                throw err;
            });

            cmake.stderr.on('data', (data) => {
                consola.error(`stderr: ${data}`)
            });

            cmake.on('close', (code) => {
                consola.log(`Exited with code: ${code}`);
                if (code === 0) resolve();
            });
        });

         */
    })
    .then(() => {
        //fs.rmdirSync(buildDir, {recursive: true})
    })
    .then(() => {
        consola.success('Done!')
    })
    .catch((err) => {
        consola.error(err);
    });
